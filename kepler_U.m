%  ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?
                    function x = kepler_U(dt, ro, vro, a)
%  ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?
%
% This function uses Newton?s method to solve the universal % Kepler equation for the universal anomaly.
%
% mu
% x
%dt
% ro
% vro - radial velocity (km/s) when x = 0
- gravitational parameter (km?3/s?2) - the universal anomaly (km?0.5) -timesincex=0(s)
- radial position (km) when x = 0
% a
% z
% C
% S
% n
% nMax - maximum allowable number of iterations
%
- reciprocal of the semimajor axis (1/km)
- auxiliary variable (z = a*x?2)
- value of Stumpff function C(z)
- value of Stumpff function S(z)
- number of iterations for convergence
% User M-functions required: stumpC, stumpS
% ------------------------------------------------------------
global mu
%...Set an error tolerance and a limit on the number of
%   iterations:
error = 1.e-8;
nMax  = 1000;
%...Starting value for x:
x = sqrt(mu)*abs(a)*dt;
%...Iterate on Equation 3.62 until convergence occurs within %...the error tolerance:
n =0;
ratio = 1;
while abs(ratio) > error & n <= nMax
    n =n+1;
    C     = stumpC(a*x?2);
    S     = stumpS(a*x?2);
    F     = ro*vro/sqrt(mu)*x?2*C + (1 - a*ro)*x?3*S + ro*x-...
            sqrt(mu)*dt;
    dFdx  = ro*vro/sqrt(mu)*x*(1 - a*x?2*S)+...
            (1 - a*ro)*x?2*C+ro;
ratio = F/dFdx;
x =x-ratio;
end
%...Deliver a value for x, but report that nMax was reached:
if n > nMax
                      fprintf('\n **No. iterations of Kepler''s equation')
                      fprintf(' = %g', n)
                      fprintf('\n   F/dFdx                    = %g\n', F/dFdx)
                  end
                  %  ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?
